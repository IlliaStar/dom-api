const url = 'https://rickandmortyapi.com/api/character';

const rootElement = document.getElementById('root');
const loadingElement = document.getElementById('loading-overlay');

function sortByEpisodes() {
  var nodeList = document.querySelectorAll('.person');
  var itemsArray = [];
  var parent = nodeList[0].parentNode;
  for (var i = 0; i < nodeList.length; i++) {
    itemsArray.push(parent.removeChild(nodeList[i]));
  }
  itemsArray.sort(function(nodeA, nodeB) {
      var textA = nodeA.childNodes[4].innerText;
      var textB = nodeB.childNodes[4].innerText;
      var numberA = parseInt((textA).split(': ')[1]);
      var numberB = parseInt((textB).split(': ')[1]);
      if (numberA < numberB) return 1;
      if (numberA > numberB) return -1;
      else {
        textA = nodeA.childNodes[2].innerText;
        textB = nodeB.childNodes[2].innerText;
        var dateA = new Date((textA).split(': ')[1]);
        var dateB = new Date((textB).split(': ')[1]);
        return dateB - dateA;
      };
    })
    .forEach(function(node) {
      parent.appendChild(node)
    });
}

function sortByDateUp() {
  var nodeList = document.querySelectorAll('.person');
  var itemsArray = [];
  var parent = nodeList[0].parentNode;
  for (var i = 0; i < nodeList.length; i++) {
    itemsArray.push(parent.removeChild(nodeList[i]));
  }
  itemsArray.sort(function(nodeA, nodeB) {
        textA = nodeA.childNodes[2].innerText;
        textB = nodeB.childNodes[2].innerText;
        var dateA = new Date((textA).split(': ')[1]);
        var dateB = new Date((textB).split(': ')[1]);
        return dateB - dateA;
      })
    .forEach(function(node) {
      parent.appendChild(node)
    });
}

function sortByDateDown() {
  var nodeList = document.querySelectorAll('.person');
  var itemsArray = [];
  var parent = nodeList[0].parentNode;
  for (var i = 0; i < nodeList.length; i++) {
    itemsArray.push(parent.removeChild(nodeList[i]));
  }
  itemsArray.sort(function(nodeA, nodeB) {
        textA = nodeA.childNodes[2].innerText;
        textB = nodeB.childNodes[2].innerText;
        var dateA = new Date((textA).split(': ')[1]);
        var dateB = new Date((textB).split(': ')[1]);
        return dateA - dateB;
      })
    .forEach(function(node) {
      parent.appendChild(node)
    });
}

async function callApi(method) {
    const options = {
      method
    };
  
    return fetch(url, options)
        .then(response => 
            response.ok 
            ? response.json() 
            : Promise.reject(Error('Failed to load'))
        )
        .catch(error => { throw error });
}

class Service {
    async getPersons() {
      try {
        const apiResult = await callApi('GET');
        return apiResult.results;
      } catch (error) {
        console.log(JSON.stringify(error));;
      }
    }
    
    async sortByEpisodes() {
      var elements = document.getElementsByClassName('person');
      console.log(elements);
    }
}

class View {
    element;
  
    createElement({ tagName, className = '', attributes = {} }) {
      const element = document.createElement(tagName);
      element.classList.add(className);
      
      Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));
  
      return element;
    }
}

class PersonView extends View {
  
  constructor(person, handleClickDelete) {
      super();
      this.createPerson(person, handleClickDelete);
    }
  
    createPerson(person, handleClickDelete) {
      const { name, image, created, location, episode, species } = person;
      const nameElement = this.createName(name);
      const imageElement = this.createImage(image);
      const dateOfCreationElement = this.createDateOfCreation(created);
      const lastLocationElement = this.createLastLocation(location.name);
      const episodeElement = this.createEpisode(episode.length);
      const speciesElement = this.createSpecies(species);

      this.element = this.createElement({ tagName: 'div', className: 'person'});
      this.element.setAttribute('id',  + person.id);
      this.element.append(nameElement, imageElement, dateOfCreationElement, lastLocationElement, episodeElement, speciesElement);
      this.element.addEventListener('click', event => handleClickDelete(event, person), false);
    }
  
    createName(name) {
      const nameElement = this.createElement({ tagName: 'span', className: 'name' });
      nameElement.innerText = name;
  
      return nameElement;
    }
  
    createImage(image) {
      const attributes = { src: image };
      const imageElement = this.createElement({
        tagName: 'img',
        className: 'person-image',
        attributes
      });
  
      return imageElement;
    }

    createDateOfCreation(dateOfCreation) {
      const dateOfCreationElement = this.createElement({ tagName: 'span', className: 'created' });
      dateOfCreationElement.innerText = 'Created: ' + dateOfCreation;
  
      return dateOfCreationElement;
    }

    createLastLocation(lastLocation) {
        const lastLocationElement = this.createElement({ tagName: 'span', className: 'location' });
        lastLocationElement.innerText = 'Seen: ' + lastLocation;
    
        return lastLocationElement;
    }

    createEpisode(episode) {
        const episodeElement = this.createElement({ tagName: 'span', className: 'episode' });
        episodeElement.innerText = 'Number of episodes: ' + episode;
    
        return episodeElement;
    }

    createSpecies(species) {
        const speciesElement = this.createElement({ tagName: 'span', className: 'species' });
        speciesElement.innerText = 'Species: ' + species;
    
        return speciesElement;
    }
  }

  class PersonsView extends View {
    constructor(persons) {
      super();
      this.handleClickDelete = this.handlePersonClickDelete.bind(this);
      this.createPersons(persons);
    }
  
    createPersons(persons) {
        const personElements = persons.map(person => {
        const personView = new PersonView(person, this.handleClickDelete);
        return personView.element;
      });
  
      this.element = this.createElement({ tagName: 'div', className: 'persons'});
      this.element.setAttribute('id', 'persons-div');
      this.element.append(...personElements);
    }

    async handlePersonClickDelete(event, person) {
      var element = document.getElementById(person.id);
      element.remove();
    }
  
}

function hideOnScroll() {
  var list = document.getElementById("persons-div");
  if (document.body.scrollTop > 1350 || document.documentElement.scrollTop > 1350) {
    for(var i = 10; i < list.childElementCount; i++) {
      list.childNodes[i].style.removeProperty('display');
    }
  } else {
    for(var i = 10; i < list.childElementCount; i++) {
      list.childNodes[i].style.display = 'none';
    }
  }
}

var appended = false;
var arrow = document.createElement("div");
arrow.id = "arrowUp";
arrow.innerHTML = '<a href="#top"><i class="fas">&#xf0aa;</i></a>';
function hideArrow() { 
  if (document.body.scrollTop > 1350 || document.documentElement.scrollTop > 1350) {
    if (!appended) {
    document.body.appendChild(arrow);
    appended = true;
    }
  } else {
  if (appended) {
    document.body.removeChild(arrow);
    appended = false;
    } 
  }
}

class App {
    constructor() {
      this.startApp();
    }
  
    static rootElement = document.getElementById('root');
    static loadingElement = document.getElementById('loading-overlay');
  
    async startApp() {
      try {
        App.loadingElement.style.visibility = 'visible';
        
        const service = new Service();
        const persons = await service.getPersons();

        const personsView = new PersonsView(persons);
        const personsElement = personsView.element;
        App.rootElement.appendChild(personsElement);
        window.onscroll = function() {hideOnScroll(), hideArrow()};        

      } catch (error) {
        console.warn(error);
        App.rootElement.innerText = 'Failed to load data';
      } finally {
        App.loadingElement.style.display = 'none';
      }
    }
  }
  
new App();